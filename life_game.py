import copy
SIZE = 5

def init_table():
    row = [False] * SIZE
    table =[]
    for i in range(SIZE):
        table.append(row.copy())
    return table

def print_table(table):
    for row in range(SIZE):
        row_list = []
        for col in range(SIZE):
            if table[col][row]:
                string = "T"
            else:
                string = "F"
            row_list.append(string)
        output = " ".join(row_list)
        print(output)

def alive(table, point):
    def count_lives(table, point):
        counter = 0
        x, y = point
        neighbours = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i != 0 or j != 0:
                    neighbours.append((x+i, y+j))
        for i, j in neighbours:
            if table[i % SIZE][j % SIZE]:
                counter += 1
        return counter
    x, y = point
    if table[x][y]:
        if count_lives(table, point) == 2 or count_lives(table, point) == 3:
            return True
    else:
        if count_lives(table, point) == 3:
            return True
    return False

class Life:
    def __init__(self, table):
        self.__table = copy.deepcopy(table)
        self.__new_table = copy.deepcopy(table)
    def __iter__(self):
        return self
    def __next__(self):
        for i in range(len(table)):
            for j in range(len(table)):
                self.__new_table[i][j] = alive(self.__table, (i, j))
        return self.__new_table

class LifeC:
    def __init__(self, table):
        self.__table = copy.deepcopy(table)
        self.__new_table = copy.deepcopy(table)
    def __iter__(self):
        return self
    def __next__(self):
        change_flag = False
        for i in range(len(table)):
            for j in range(len(table)):
                self.__new_table[i][j] = alive(self.__table, (i, j))
                if self.__new_table[i][j] != self.__table[i][j]:
                    change_flag = True
        print(change_flag)
        if not change_flag:
            raise StopIteration
        else:
            self.__table = copy.deepcopy(self.__new_table)
            return self.__new_table

table = init_table()
table[3][3] = True
table[1][3] = True
table[2][2] = True
table[3][2] = True
table[4][1] = True

print("before:")
print_table(table)

life = LifeC(table)
k = 0
while k < 7:
    print("*"*20)
    print_table(next(life))
    k += 1