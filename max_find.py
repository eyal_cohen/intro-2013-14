def max_find(lst):
    start = 0
    end = len(lst) - 1
    while start < end:
        mid = (start + end) // 2
        if lst[mid] < lst[start]:
            end = mid -1
        else:
            start = mid
        if end-start <= 1:
            if lst[start] < lst[end]:
                return lst[end]
            else:
                return lst[start]

lst = [85, 94, 96, 80, 28, 14, 10, 3, 3]
lst = [88, 99, 2, 2, 2, 2, 1]
#lst = [2, 2, 2, 1, 1, 1]
#lst = [2, 2, 2, 3, 3, 3]
print(max_find(lst))