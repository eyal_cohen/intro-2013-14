#import pdb

ROW_SIZE = 9
FILENAME = "soduku_board"

def init_board():
    row = [0] * ROW_SIZE
    board =[]
    for i in range(ROW_SIZE):
        board.append(row.copy())
    return board

def legal_placement(board, col, row):
    for i in range(ROW_SIZE):
        if i != row:
            if board[col][i] == board[col][row]:
                return False
    for i in range(ROW_SIZE):
        if i != col:
            if board[i][row] == board[col][row]:
                return False
    for i in range((col//3)*3, ((col//3)*3)+3):
        for j in range((row//3)*3, ((row//3)*3)+3):
            if board[i][j] == board[col][row]:
                if i != col or j != row:
                    return False
    return True

def print_board(board, filename):
    f = open(FILENAME, "w")
    for row in range(ROW_SIZE):
        row_list = []
        for col in range(ROW_SIZE):
            row_list.append(str(board[col][row]))
        output = ",".join(row_list)
        f.write(output + "\n")

def play_game(board):
    if play_game_helper(board, 0, 0):
        print_board(board, FILENAME)
    else:
        print("There’s no solution for this board")

def play_game_helper(board, col, row):
    for i in range(1, 10):
        board[col][row] = i
        if legal_placement(board, col, row):
            if col < ROW_SIZE - 1:
                if play_game_helper(board, col+1, row):
                    return True
            elif row < ROW_SIZE - 1:
                if play_game_helper(board, 0, row+1):
                    return True
            else:
                return True
    board[col][row] = 0
    return False
                

#board = [[x for x in range(1, 10)] for i in range(9)]
board = init_board()
play_game(board)
print_board(board, FILENAME)
