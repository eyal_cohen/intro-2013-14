def merge(x, y):
    def helper(x, y):
        cur, prev = x, x
        while y != None:
            next_y = y.next
            while y.data >= cur.data:
                prev = cur
                cur = cur.next
            prev.next = y
            y.next = cur
            prev = y
            y = next_y
        return x
    if y.data < x.data:
        return helper(y, x)
    else:
        return helper(x, y)