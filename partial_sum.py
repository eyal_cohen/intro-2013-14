#counter = 0
def p_s(lst, num):
    def helper(lst, num, output):
        #global counter
        #counter += 1
        if len(lst) == 0:
            if num == 0:
                return True
        else:
            if helper(lst[1:], num, output):
                return True
            elif helper(lst[1:], num-lst[0], output):
                output.append(lst[0])
                return True
            return False
    output = []
    if helper(lst, num, output):
        return output

lst = range(5)
print(p_s(lst, 100))
#print("counter: " + str(counter))